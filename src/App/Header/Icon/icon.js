import React from 'react'

import './icon.css'

const Icon = () =>

<div className="icon">
<span className="drawic drawic-share"></span>
<span className="drawic drawic-email"></span>
<span className="drawic drawic-facebook"></span>
<span className="drawic drawic-favorite"></span>
<span className="drawic drawic-home"></span>
<span className="drawic drawic-instagram"></span>
<span className="drawic drawic-like"></span>
</div>

export default Icon