import React from 'react'

import logo from './logo.png'
import './logo.css'

const Logo = () =>
<div className="logo">
    <img src={logo} alt="" />
</div>


export default Logo