import React from 'react'

import Logo from './Logo/Logo'
import Icon from './Icon/Icon'

import './header.css'

const Header = () =>
<header className="header">
   <div className="container">
        <div className="row">
             <div className="col-md-8">
                    <Logo />
             </div>
             <div className="co-md-4 icon-drawic">
                   <Icon />

             </div>

        </div>

   </div>
</header>

export default Header
