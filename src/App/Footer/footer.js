import React from 'react' 

const Footer = () => {
	return (
	<footer className="footer">
         <div className="container">
            <div className="row">
                 <div className="footer-border">
                     <div className="col-md-3">
                         <div className="footer-date-createsite">
                            18+ © Украина.ру, 2018.
                         </div>
                     </div>
                     <div className="col-md-9">
                         <div className="footer-description">
                             Все результаты интеллектуальной деятельности, размещенные на сайте https://ukraina.ru/, охраняются в соответствии с правом Российской Федерации или иным применимым правом. Использование произведений, правообладателем которых являются третьи лица, размещенных на сайте https://ukraina.ru/, запрещено без получения разрешения правообладателя.
                             По всем вопросам, возникающим при использовании сайта, в том числе связанным с нарушением прав использования результатов интеллектуальной деятельности просим обращаться editors@ukraina.ru
                             Мнение владельца сайта может не совпадать с точкой зрения авторов публикаций
                         </div>
                     </div>
                </div>
            </div>
        </div> 
    </footer>
	)
}
	

export default Footer 