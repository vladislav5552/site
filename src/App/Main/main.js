import React from 'react'
import ActualyNewsList from './ActualyNews/ActualyNewsList'
import PartnerNewsList from './PartnerNews/PartnerNewsList'
import SmiNewsList from './SmiNews/SmiNewsList'

const Main = () => 

<main className="main">
     <div className="container">
             
                 <div className="col-md-8">   
                   <ActualyNewsList />     
                
             </div> 
             <div className="col-md-4"> 
                 <div className="row">
                  <PartnerNewsList />
                 </div>    
              </div>
             <div className="col-md-12">
                 <div className="row"> 
                  <SmiNewsList/>
                 </div>
             </div>     
     </div>      
</main>

export default Main