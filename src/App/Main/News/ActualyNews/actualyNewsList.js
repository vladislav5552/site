import React from 'react'
import ActualyNewsListItem from './ActualyNewsListItem'
import './ActualyNewsList.css'

const ActualyNewsList = () => {
return (
     <div className="title">Новости</div>
     <div>
               {
                    actyalyNews.map(({
                        id,
                        time,
                        description,
                        image,

                    })=>(
                            <ActualyNewsListItem
                                id={id}
                                description={description}
                                image={image}
                                time={time}
                            />
                         )) 
                    } 
</div>
<div className="submit">Загрузить еще</div>
)
}

export default ActualyNewsList